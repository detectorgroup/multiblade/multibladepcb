/*
based on:  https://github.com/vitos73/webthermometer

REQUIRED Libraries: 
         https://github.com/jcw/ethercard  
         INA226  BY ROB TILLAART
      
      Device configured to use only DHCP address
It can be accesses as      
      index page - http://device_ip_from_dhcp/   e.g http://192.168.0.100
      single sensor channel  http://device_ip_from_dhcp/?channel=X   where X is {0..5} e.g http://192.168.0.100/?channel=4
      use serial monitor to read an ip or check your router/server's dhcp lease logs
*/

#include <EtherCard.h>
#include "INA226.h"

#define DEBUG true
#define DS_PIN 2   // pin for sensors data, needs to be pulled up to 5v via 4.7kOhm resistor
#define CS_PIN 10  // CS pin of ENC28J60
#define SWITCH_PIN 5
#define LED 13
#define IntNetwork 7
bool switchstatus = true;


//change library or define 4 inas and so on.
const int numDevices = 4;  // Number of INA devices
INA226 INA[numDevices] = { INA226(0x40), INA226(0x41), INA226(0x44), INA226(0x45) };
bool DevPresent[numDevices] = { true, true, true, true };

uint8_t sensorChannels[numDevices] = { 0, 1, 2, 3 };



// tell onewire which pin is for sensors

// this sketch is only for DHCP configuration
// ENC28J60 doesn't have own MAC address, so MAC is required!
//static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x34 };
static byte mymac[] = { 0xD4, 0x28, 0xB2, 0xFF, 0xA0, 0xA8 };

uint8_t sens_count;

// allocate 900 bytes of RAM to programm IO buffer for ethernet
byte Ethernet::buffer[900];
BufferFiller bfill;


// litle style for main page
// remember! - you have only 900 bytes for answer footprint
const char css[] PROGMEM = "\r\n<style>* {font-size:18pt;font-family:Tahoma,Verdana,Arial;color:#777;}</style>\r\n";

// header for mainpage
const char http200b[] PROGMEM =
  "HTTP/1.0 200 OK\r\n"
  "Content-Type: text/html\r\n"
  "Connection: close\r\n"  // The connection will be closed after completion of the response
  "Refresh: 3\r\n"         //
  "Pragma: no-cache\r\n"
  "\r\n";

const char header[] PROGMEM =
  "<!DOCTYPE html>\r\n"
  "<html>\r\n<head>\r\n<title>WEB INA LV DIST</title>\r\n</head>\r\n"
  "<body>";

// footer for mainpage
const char http200e[] PROGMEM = "\r\n</body>\r\n</html>";

// JS to reload main page every 5s
const char js_reload[] PROGMEM = "<script language=\"Javascript\">setTimeout(function(){window.location.reload(50000);}, );</script>";
//const char js_reload[] PROGMEM = "";

const char js_redirect[] PROGMEM =
  "<script>\r\n"
  "location.replace(\"/\")\r\n"
  "</script>\r\n";


float Voltage[4];
float Current[4];
float Power[4];


// *********************************************SETUP***************************************************************************
// *********************************************SETUP***************************************************************************


void setup() {
  delay(1000);

  Serial.begin(9600);
  if (DEBUG) Serial.println("beginning");

  if (ether.begin(sizeof Ethernet::buffer, mymac, CS_PIN) == 0)
    if (DEBUG) Serial.println("Failed to access Ethernet controller");

  delay(300);

  if (!ether.dhcpSetup())
    if (DEBUG) Serial.println("DHCP failed");

  ether.printIp("MyIP: ", ether.myip);
  ether.printIp("GW:   ", ether.gwip);

  delay(100);

  pinMode(SWITCH_PIN, OUTPUT);
  pinMode(LED, OUTPUT);

  digitalWrite(SWITCH_PIN, HIGH);
  digitalWrite(LED, HIGH);
  switchstatus = true;

  Wire.begin();

  for (int i = 0; i < numDevices; i++) {
    if (!INA[i].begin()) {
      if (DEBUG) Serial.print("Failed to initialize INA226 at address 0x");
      if (DEBUG) Serial.println(INA[i].getAddress(), HEX);
      DevPresent[i] = false;
    }
    INA[i].setMaxCurrentShunt(15, 0.001, true);
  }

  //attachInterrupt(digitalPinToInterrupt(IntNetwork), network, FALLING);  //low?   seems  to be causing problems after a while.
}

// *********************************************LOOP***************************************************************************
// *********************************************LOOP***************************************************************************
// *********************************************LOOP***************************************************************************
int counter = 0;

void loop() {

  delay(100);
  counter++;

  if (switchstatus == true)
    digitalWrite(SWITCH_PIN, HIGH);
  else
    digitalWrite(SWITCH_PIN, LOW);

  if (counter == 50) {
    counter = 0;
    if (DEBUG) Serial.println("\nVOLTS\tCURRENT\tPOWER");

    for (uint8_t i = 0; i < numDevices; i++) {
      if (DevPresent[i]) {
        Voltage[i] = INA[i].getBusVoltage();
        if (Voltage[i] < -50 || Voltage[i] > 50)
          Voltage[i] = 0;
        
        Current[i] = INA[i].getCurrent_mA()/1000;
        if (Current[i] < 0)
          Current [i] = 0;

        Power[i] = INA[i].getPower_mW()/1000;
          if (Power[i] < 0)
            Power[i] = 0;


        if (DEBUG) {
          Serial.print(counter);
          Serial.print("\t");
          Serial.print(Voltage[i]);
          Serial.print("\t");
          Serial.print(Current[i]);
          Serial.print("\t");
          Serial.print(Power[i]);
          Serial.println();
        }
      }
    }
  }


  network();

  //delay(100);

  /*if (DEBUG) Serial.println("checking connection");

  if (!ether.dnsLookup("www.google.com"))
    if (DEBUG) Serial.println("DNS failed");
  else
    ether.printIp("Server: ", ether.hisip);

  delay(20000);

  if (!ether.dnsLookup("www.google.com"))
    if (DEBUG) Serial.println("DNS failed");
  else
    ether.printIp("Server: ", ether.hisip);

    if (DEBUG) Serial.println("Reseteup");

  if (!ether.dhcpSetup())
    if (DEBUG) Serial.println("DHCP failed");
  ether.printIp("MyIP: ", ether.myip);
  ether.printIp("GW:   ", ether.gwip);
  */
}





// *********************************************functions***************************************************************************
// *********************************************functions***************************************************************************
// *********************************************functions***************************************************************************
// *********************************************functions***************************************************************************

void network() {

  //if (DEBUG) Serial.println("Request incomming !!! ");

  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);

  if (DEBUG)
    if (len > 0) Serial.print(len);
  if (DEBUG)
    if (len > 0) Serial.print("\t");
  if (DEBUG)
    if (len > 0) Serial.println(pos);

  if (pos) {
    bfill = ether.tcpOffset();
    char *data = (char *)Ethernet::buffer + pos;
    if (strncmp("GET /", data, 5) != 0) {

      mainPage();
    }


    else {

      data += 5;
      if (data[0] == ' ') {
        mainPage();
      }


      else if (strncmp("?Switch=ON ", data, 11) == 0) {
        // digitalWrite(SWITCH_PIN, LOW);
        switchstatus = true;
        //bfill.emit_p(PSTR("\r\n <meta http-equiv=\"refresh\" content=\"0; url=/\" /> "));

        // <br /><a href=\"?Switch=OFF\">Switch:</a> LV DISTRIBUTOR IS ENABLED
        if (DEBUG) Serial.println("Switching on");
        backtomain();
      }


      else if (strncmp("?Switch=OFF ", data, 12) == 0) {
        //digitalWrite(SWITCH_PIN, HIGH);
        switchstatus = false;
        //bfill.emit_p(PSTR("\r\n <meta http-equiv=\"refresh\" content=\"0; url=/\" /> "));
        if (DEBUG) Serial.println("Switching on");
        backtomain();

      } else {
        mainPage();
      }
    }
    ether.httpServerReply(bfill.position());  // send http response
  }
  //if (DEBUG) Serial.println("Request ended. ");
}






void mainPage() {
  char V_string[8];
  char I_string[8];
  char P_string[8];

  delay(5);
  char channel_str[2];


  if (numDevices > 0) {
    bfill.emit_p(PSTR("$F"), http200b);
    bfill.emit_p(PSTR("$F"), header);
    bfill.emit_p(PSTR("$F"), css);

    if (switchstatus == true)
      bfill.emit_p(PSTR("\r\n<br /><a href=\"?Switch=OFF\">Switch:</a> LV DISTRIBUTOR IS ENABLED "));
    else
      bfill.emit_p(PSTR("\r\n<br /><a href=\"?Switch=ON\">Switch:</a>LV DISTRIBUTOR IS DISABLED "));

    for (uint8_t i = 0; i < numDevices; i++) {
      if (DevPresent[i]) {

        //values for voltage current and power are updated globably

        dtostrf(Voltage[i], 3, 2, V_string);
        dtostrf(Current[i], 3, 2, I_string);
        dtostrf(Power[i], 3, 2, P_string);
        dtostrf(i + 1, 1, 0, channel_str);
        bfill.emit_p(PSTR("\r\n<br />Channel: $S</a>, Voltage: $SV, Current $SA, Power $SW "), channel_str, V_string, I_string, P_string);
      }
    }

    //bfill.emit_p(PSTR("$F"), js_reload);
    bfill.emit_p(PSTR("$F"), http200e);
  } else {
    bfill.emit_p(PSTR("$F"), http200b);
    bfill.emit_p(PSTR("$F"), css);
    bfill.emit_p(PSTR("NO SENSORS FOUND. CHECK WIRING & CONNECTION AND RESTART!!"));
    //bfill.emit_p(PSTR("$F"), js_reload);
    bfill.emit_p(PSTR("$F"), http200e);
  }
}

void backtomain()

{
  bfill.emit_p(PSTR("$F"), http200b);
  bfill.emit_p(PSTR("$F"), header);

  bfill.emit_p(PSTR("$F"), js_redirect);
  bfill.emit_p(PSTR("$F"), js_reload);
  bfill.emit_p(PSTR("$F"), http200e);
}
