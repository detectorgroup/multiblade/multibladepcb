import requests
import time

# Arduino server IP address and port
arduino_ip = "192.168.1.4"  # Replace with your Arduino's IP address
arduino_port = 80

# File to log data
log_file_path = "sensor_log.txt"

def log_sensor_data():
    while True:
        try:
            # Make a GET request to the Arduino server
            response = requests.get(f"http://{arduino_ip}:{arduino_port}")

            # Check if the request was successful (status code 200)
            if response.status_code == 200:
                # Parse the HTML response to extract sensor data
                # Note: This assumes that the sensor data is formatted in a specific way in the HTML response
               
                # Extracting values for Sensor 1
                temperature1 = float(response.text.split("Temperature1: ")[1].split(" degrees C")[0])
                pressure1 = float(response.text.split("Pressure1: ")[1].split(" hPa")[0])
                humidity1 = float(response.text.split("Humidity1: ")[1].split(" %rH")[0])

                # Extracting values for Sensor 2
                temperature2 = float(response.text.split("Temperature2: ")[1].split(" degrees C")[0])
                pressure2 = float(response.text.split("Pressure2: ")[1].split(" hPa")[0])
                humidity2 = float(response.text.split("Humidity2: ")[1].split(" %rH")[0])

                # Log the data to a text file
                with open(log_file_path, mode="a") as text_file:
                    text_file.write(f"{time.strftime('%Y-%m-%d %H:%M:%S')} - Sensor 1: Temperature={temperature1}°C, Pressure={pressure1}hPa, Humidity={humidity1}% | Sensor 2: Temperature={temperature2}°C, Pressure={pressure2}hPa, Humidity={humidity2}%\n")

                print(f"Data logged: Sensor 1 - Temperature={temperature1}°C, Pressure={pressure1}hPa, Humidity={humidity1}%, Sensor 2 - Temperature={temperature2}°C, Pressure={pressure2}hPa, Humidity={humidity2}%")

            else:
                print(f"Failed to retrieve data from the server. Status code: {response.status_code}")

            # Wait for some time before making the next request (e.g., every 5 seconds)
            time.sleep(5)

        except Exception as e:
            print(f"An error occurred: {e}")

if __name__ == "__main__":
    log_sensor_data()
