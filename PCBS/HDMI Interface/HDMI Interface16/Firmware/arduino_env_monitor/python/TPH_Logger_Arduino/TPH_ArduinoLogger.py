#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 10:40:50 2023

@author: francescopiscitelli & Sara Al Tawil 
"""

###############################################################################
###############################################################################
########    V3 2024/02/15       ###############################################
###############################################################################
###############################################################################

import requests
import time
from datetime import datetime
import os

# import numpy as np

class logger():
    
    def __init__(self):
        ###############################################################################
        ###############################################################################
        
        currentLoc = os.path.abspath(os.path.dirname(__file__))
        
        ###############################################################################
        ###############################################################################
        
        # time interval to log (s)
        self.timeInterval = 900
        
        # split files every day at 00:00
        
        # folder to dump log files 
        self.folderPath = currentLoc+'/LogFiles/'
        
        # file base name 
        self.fileNameBase = 'TPHlog_MB.test'
        
        # Arduino server IP address and port
        self.arduino_ip   = "172.30.244.46"  # Replace with your Arduino's IP address
        self.arduino_port = 80
        

        ###############################################################################
        ###############################################################################
        
        nowTime = datetime.now()
        self.current_date = nowTime.strftime("%Y-%m-%d")
        self.current_time = nowTime.strftime("%H:%M:%S")
  
        self.writeFailureLogFile('Logging restarted')
        
        print("--------------------------------------")
        
        while True:
            
            try:
                
                # Make a GET request to the Arduino server
                response = requests.get(f"http://{self.arduino_ip}:{self.arduino_port}")
                
                # Check if the request was successful (status code 200)
                if response.status_code == 200:
                    # Parse the HTML response to extract sensor data
                    # Note: This assumes that the sensor data is formatted in a specific way in the HTML response
                    
                    texxt  = response.text.split("SENSOR 1")[1].split("SENSOR 2")
                    # Extracting values for Sensor 1
                    temperature1 = float(texxt[0].split("Temperature: ")[1].split(" degrees C")[0])
                    pressure1    = float(texxt[0].split("Pressure: ")[1].split(" hPa")[0])
                    humidity1    = float(texxt[0].split("Humidity: ")[1].split(" %rH")[0])
        
                    # Extracting values for Sensor 2
                    temperature2 = float(texxt[1].split("Temperature: ")[1].split(" degrees C")[0])
                    pressure2    = float(texxt[1].split("Pressure: ")[1].split(" hPa")[0])
                    humidity2    = float(texxt[1].split("Humidity: ")[1].split(" %rH")[0])
        
                    # # Log the data to a text file
                    # with open(log_file_path, mode="a") as text_file:
                    #     text_file.write(f"{time.strftime('%Y-%m-%d %H:%M:%S')} - Sensor 1: Temperature={temperature1}°C, Pressure={pressure1}hPa, Humidity={humidity1}% | Sensor 2: Temperature={temperature2}°C, Pressure={pressure2}hPa, Humidity={humidity2}%\n")
        
                    # print(f"Data logged: Sensor 1 - Temperature={temperature1}°C, Pressure={pressure1}hPa, Humidity={humidity1}%, Sensor 2 - Temperature={temperature2}°C, Pressure={pressure2}hPa, Humidity={humidity2}%")
        
                    # temperature1 = 23+np.random.rand()
                    # humidity1 = 40+np.random.rand()
                    # pressure1 = 950+np.random.rand()
                    # temperature2 = 26+np.random.rand()
                    # humidity2 = 46+np.random.rand()
                    # pressure2 = 956+np.random.rand()
                    
                    nowTime = datetime.now()
                    
                    self.current_date = nowTime.strftime("%Y-%m-%d")
                    self.current_time = nowTime.strftime("%H:%M:%S")
                    
                    ####### Output data to screen ###############
                    print("date: %s" %self.current_date)
                    print("time: %s" %self.current_time)
                    print("Temperature 1 in Celsius : %.2f C" %temperature1)
                    print("Pressure 1 is : %.2f mbar" %pressure1)
                    print("Relative Humidity 1: %.2f %%" %humidity1)
                    print("Temperature 2 in Celsius : %.2f C" %temperature2)
                    print("Pressure 2 is : %.2f mbar" %pressure2)
                    print("Relative Humidity 2: %.2f %%" %humidity2)
                    print("--------------------------------------")
                    #############################################
                    
                    fileh = self.folderPath+self.fileNameBase+'_'+self.current_date+'.txt'
                    
                    temp = '%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n' % (temperature1, pressure1, humidity1, temperature2, pressure2, humidity2)
                    stringa = self.current_time+'\t'+temp
                        
                    if os.path.isfile(fileh):
                       # if the file already exists open it appending
                       fo = open(fileh, "a")
                    else:
                       # open/create a new file and add the field names
                       fo = open(fileh, "w")
                       fo.writelines("Log File for date: " + self.current_date + "\n")
                       fo.writelines("columns: time, T(C), P(mBar), RH(%), T(C), P(mBar), RH(%) \n")
                                
                    fo.writelines(stringa)
                    
                    fo.close()     
                    
                    time.sleep(self.timeInterval)
                    
                else:
                
                    print(f"Failed to retrieve data from the server. Status code: {response.status_code}")
        
                    # Wait for some time before making the next request (e.g., every 5 seconds)
                    time.sleep(self.timeInterval)
                    print("--------------------------------------")
        
            except Exception as e:
        
                   
                nowTime = datetime.now()
             
                self.current_date = nowTime.strftime("%Y-%m-%d")
                self.current_time = nowTime.strftime("%H:%M:%S")
                print("date: %s" %self.current_date)
                print("time: %s" %self.current_time)
                print(f"An error occurred: {e}")
                print("-> probably web server is off line!")
                
                self.writeFailureLogFile('Log attempt failed')
                
                print("--------------------------------------")
                
                time.sleep(self.timeInterval)
                
                

###############################################################################
###############################################################################

    def writeFailureLogFile(self,stringa):
        
        filepower = self.folderPath+self.fileNameBase+'_'+self.current_date+'_DataRetrievalFailureLog.txt' 
        if os.path.isfile(filepower):
            # if the file already exists open it appending
            fop = open(filepower, "a")
        else:
             # open/create a new file and add the field names
            fop = open(filepower, "w")
            
        stringa1 =  stringa +': '+  self.current_date+ '\t' + self.current_time +'\n'
         
        fop.writelines(stringa1)
         
        fop.close()
 
###############################################################################
###############################################################################        
  
if __name__ == '__main__':       
    
    logger()