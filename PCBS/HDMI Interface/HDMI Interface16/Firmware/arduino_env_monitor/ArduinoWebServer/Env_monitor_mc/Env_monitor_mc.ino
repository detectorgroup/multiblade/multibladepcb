/*
based on:  https://github.com/vitos73/webthermometer

REQUIRED Libraries: 
         https://github.com/jcw/ethercard  
         INA226
      
      Device configured to use only DHCP address
It can be accesses as      
      index page - http://device_ip_from_dhcp/   e.g http://192.168.0.100
      single sensor channel  http://device_ip_from_dhcp/?channel=X   where X is {0..5} e.g http://192.168.0.100/?channel=4
      use serial monitor to read an ip or check your router/server's dhcp lease logs
*/

#include <EtherCard.h>
#include <Adafruit_MS8607.h>
#include <ArduinoJson.h>
#include <LiquidCrystal_I2C.h>


#define DEBUG true

#define CS_PIN 10  // CS pin of ENC28J60

// set it to true if the lcd display is connected.
#define DISPLAY false


// Initialize the LCD with the I2C address (0x3F) and dimensions (16 columns, 2 rows)
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Sensor objects
Adafruit_MS8607 sensorA;  // Left sensor
Adafruit_MS8607 sensorB;  // Right sensor

sensors_event_t temp1, pressure1, humidity1;
sensors_event_t temp2, pressure2, humidity2;

// Transistor control pins for sensors
int pinA = 4;
int pinB = 5;

bool s1found = false;
bool s2found = false;

bool selectSensor = true;

// this sketch is only for DHCP configuration
// ENC28J60 doesn't have own MAC address, so MAC is required!
//static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x34 };
static byte mymac[] = { 0x74, 0x69, 0x69, 0x2D, 0x30, 0x31 };

uint8_t sens_count;

// allocate 900 bytes of RAM to programm IO buffer for ethernet
byte Ethernet::buffer[900];
BufferFiller bfill;


// litle style for main page
// remember! - you have only 900 bytes for answer footprint
const char css[] PROGMEM = "\r\n<style>* {font-size:18pt;font-family:Tahoma,Verdana,Arial;color:#777;}</style>\r\n";

// header for mainpage
const char http200b[] PROGMEM =
  "HTTP/1.0 200 OK\r\n"
  "Content-Type: text/html\r\n"
  "Pragma: no-cache\r\n"
  "\r\n"
  "<!DOCTYPE html>\r\n"
  "<html>\r\n<head>\r\n<title>WEB INA LV DIST</title>\r\n</head>\r\n"
  "<body>";

// footer for mainpage
const char http200e[] PROGMEM = "\r\n</body>\r\n</html>";

// JS to reload main page every 60s
const char js_reload[] PROGMEM = "<script language=\"Javascript\">setTimeout(function(){window.location.reload(1);}, 60000);</script>";

// text/plain header for answer - for zabbix you need only text without any html tags
const char http_OK[] PROGMEM =
  "HTTP/1.0 200 OK\r\n"
  "Content-Type: text/plain\r\n"
  "Pragma: no-cache\r\n\r\n";



//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------

void setup() {
  Serial.begin(115200);
  Serial.println("beginning");

  //ethernet init

  if (ether.begin(sizeof Ethernet::buffer, mymac, CS_PIN) == 0)
    Serial.println("Failed to access Ethernet controller");
  delay(300);
  if (!ether.dhcpSetup())
    Serial.println("DHCP failed");
  ether.printIp("MyIP: ", ether.myip);
  ether.printIp("GW:   ", ether.gwip);

  delay(100);

  //sensor initialization

  Wire.begin();

  ActS1();
  if (sensorA.begin()) {
    Serial.println("Sensor 1 found");
    s1found = true;
  } else
    Serial.println("Sensor 1 not found :(");

  ActS2();
  if (sensorB.begin()) {
    Serial.println("Sensor 2 found");
    s2found = true;
  } else
    Serial.println("Sensor 2 not found :(");
}


//////////--------------------------------------------------------------

void loop() {

  delay(100);

  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);

  if (pos) {
    bfill = ether.tcpOffset();
    char *data = (char *)Ethernet::buffer + pos;
    if (strncmp("GET /", data, 5) != 0) {

      mainPage();
    }


    else {

      data += 5;
      if (data[0] == ' ') {
        mainPage();
      }

      else {
        // where is no 404 handlers, for every invalid page we show main page
        mainPage();
      }
    }
    ether.httpServerReply(bfill.position());  // send http response
  }
}





//------------------------
//------------------------


void mainPage() {
  char T_string[8];
  char H_string[8];
  char P_string[8];

  delay(5);
  char channel_str[2];


  if (s1found == true && s1found == true) {
    ActS1();
    bfill.emit_p(PSTR("$F"), http200b);
    bfill.emit_p(PSTR("$F"), css);

    for (uint8_t i = 0; i < 2; i++) {
      float Temperature = 0.0;  //get temperature here
      float Humidity = 0.0;     //get humidity
      float Presure = 0.0;      // get presure

      dtostrf(Temperature, 3, 2, T_string);
      dtostrf(Humidity, 3, 2, H_string);
      dtostrf(Presure, 3, 2, P_string);
      dtostrf(i, 1, 0, channel_str);
      bfill.emit_p(PSTR("\r\n<br /><a href=\"?channel=$S\">Channel: $S</a>, Temperature: $SV, Humidity $SA, Presure $SW "), channel_str, channel_str, T_string, H_string, P_string);
      ActS2();
    }

  } else {
    bfill.emit_p(PSTR("$F"), http200b);
    bfill.emit_p(PSTR("$F"), css);
    bfill.emit_p(PSTR("NO SENSORS FOUND. CHECK WIRING & CONNECTION"));
  }
  bfill.emit_p(PSTR("$F"), js_reload);
  bfill.emit_p(PSTR("$F"), http200e);
}


//-----------------------------------------------


void ActS1(void) {
  delay(5);
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, HIGH);
}

void ActS2(void) {
  delay(5);
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, LOW);
}