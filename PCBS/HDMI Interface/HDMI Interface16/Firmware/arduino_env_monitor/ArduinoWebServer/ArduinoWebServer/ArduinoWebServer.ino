/*-----------------------------------
Requires libraries to be installed to work:
Adafruit_MS8607 1.0.4 with adafruit_busio and adafruit_unified_sensor
Ethernet (intalled in the folder)
liquidcrystal i2c : by frank de Brabander  1.1.2
arduinojson: by Benoit Blanchon 6.21.3
*/

#include <Wire.h>
#include <Adafruit_MS8607.h>
#include <ArduinoJson.h>
#include <Ethernet.h>
#include <LiquidCrystal_I2C.h>



#define DEBUG true
// set it to true if the lcd display is connected.
#define DISPLAY false

// Initialize the LCD with the I2C address (0x3F) and dimensions (16 columns, 2 rows)
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Replace with your Ethernet shield MAC address
byte mac[] = { 0xD4, 0x28, 0xB2, 0xFF, 0xA0, 0xA7 };  // Replace with your Ethernet shield MAC address
//IPAddress ip(192, 168, 1, 7);  // Set your desired IP address

// Sensor objects
Adafruit_MS8607 sensorA;  // Left sensor
Adafruit_MS8607 sensorB;  // Right sensor

sensors_event_t temp1, pressure1, humidity1;
sensors_event_t temp2, pressure2, humidity2;

// Transistor control pins for sensors
int pinA = 4;
int pinB = 5;

bool s1found = false;
bool s2found = false;

bool selectSensor = true;

EthernetServer server(80);

void setup() {

  delay(3000);
  Serial.begin(115200);
  Serial.println("MS8607 Sensor test");

  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);

  delay(250);


  Ethernet.begin(mac);
  server.begin();
  Serial.println("Server is ready at:");
  Serial.println(Ethernet.localIP());


  delay(250);


  ActS1();
  if (sensorA.begin()) {
    Serial.println("Sensor 1 found");
    s1found = true;
  } else
    Serial.println("Sensor 1 not found :(");

  ActS2();
  if (sensorB.begin()) {
    Serial.println("Sensor 2 found");
    s2found = true;
  } else
    Serial.println("Sensor 2 not found :(");


  //INIT network





  //INIT display
  if (DISPLAY == true) {
    lcd.init();
    lcd.backlight();
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Server is ready");
  }
}



//******************************************************************************************************************************************
//******************************************************************************************************************************************
//******************************************************************************************************************************************

void loop() {

  if (s1found == true) {  // Read data from left sensor (sensor1)
    ActS1();
    sensorA.getEvent(&pressure1, &temp1, &humidity1);
  }

  if (s2found == true) {
    // Read data from left sensor (sensor2)
    ActS2();
    sensorB.getEvent(&pressure2, &temp2, &humidity2);
  }

  if (DISPLAY == true) {
    //sensor 1
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("T1: ");
    lcd.print(temp1.temperature, 1);
    lcd.setCursor(0, 1);
    lcd.print("P1: ");
    lcd.print(pressure1.pressure, 1);
    lcd.setCursor(8, 0);
    lcd.print("H1: ");
    lcd.print(humidity1.relative_humidity, 1);
    // sensor 2;
    lcd.setCursor(0, 0);
    lcd.print("T2: ");
    lcd.print(temp2.temperature, 1);
    lcd.setCursor(0, 1);
    lcd.print("P2: ");
    lcd.print(pressure2.pressure, 1);
    lcd.setCursor(8, 0);
    lcd.print("H2: ");
    lcd.print(humidity2.relative_humidity, 1);
  }

  if (DEBUG == true) {
    Serial.print("Sensor 1: Temp: ");
    Serial.print(temp1.temperature);
    Serial.print("  Hum ");
    Serial.print(humidity1.relative_humidity);
    Serial.print("  Pres: ");
    Serial.print(pressure1.pressure);

    Serial.print("   **   ");

    Serial.print("Sensor 2: Temp: ");
    Serial.print(temp2.temperature);
    Serial.print("  Hum ");
    Serial.print(humidity2.relative_humidity);
    Serial.print("  Pres: ");
    Serial.print(pressure2.pressure);
    Serial.println(" ");
  }



  EthernetClient client = server.available();
  if (client) {
    Serial.println("New client connected");
    boolean currentLineIsBlank = true;
    String currentLine = "";

    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        currentLine += c;

        if (c == '\n' && currentLineIsBlank) {
          // Send a response to the client
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println();

          client.println("<html>");
          client.println("<head><title>MS8607 Sensor Data</title></head>");
          client.println("<body>");
          client.println("<h1>MS8607 Sensor Data</h1>");


          sendDataToClient(client, temp1, pressure1, humidity1, temp2, pressure2, humidity2);
          break;
        }
        if (c == '\n') {
          currentLineIsBlank = true;
          currentLine = "";
        } else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(1000);
    client.stop();
    Serial.println("Client disconnected");
  }
      delay(1000);

}

void sendDataToClient(EthernetClient client, sensors_event_t temp, sensors_event_t pressure, sensors_event_t humidity, sensors_event_t temp2, sensors_event_t pressure2, sensors_event_t humidity2) {
  // Send HTML response
  /*client.println("<html>");
  client.println("<head><title>MS8607 Sensor Data</title></head>");
  client.println("<body>");
  client.println("<h1>MS8607 Sensor Data</h1>");*/
  client.println("<h2>SENSOR 1</h2>");
  client.print("Temperature: ");
  client.print(temp.temperature, 1);
  client.println(" degrees C<br>");
  client.print("Pressure: ");
  client.print(pressure.pressure, 1);
  client.println(" hPa<br>");
  client.print("Humidity: ");
  client.print(humidity.relative_humidity, 1);
  client.println(" %rH<br>");

  // Right sensor (sensorB) data
  client.println("<h2>SENSOR 2</h2>");
  client.print("Temperature: ");
  client.print(temp2.temperature, 1);
  client.println(" degrees C<br>");
  client.print("Pressure: ");
  client.print(pressure2.pressure, 1);
  client.println(" hPa<br>");
  client.print("Humidity: ");
  client.print(humidity2.relative_humidity, 1);
  client.println(" %rH<br>");


  // JavaScript auto-refresh code
  client.println("<script>");
  client.println("setTimeout(function() {");
  client.println("  location.reload();");  // Reload the page after a delay (e.g., every 5 seconds)
  client.println("}, 5000);");             // Adjust the refresh interval as needed (in milliseconds)
  client.println("</script>");

  client.println("</body>");
  client.println("</html>");
}



void ActS1(void) {
  delay(5);
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, HIGH);
}

void ActS2(void) {
  delay(5);
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, LOW);
}