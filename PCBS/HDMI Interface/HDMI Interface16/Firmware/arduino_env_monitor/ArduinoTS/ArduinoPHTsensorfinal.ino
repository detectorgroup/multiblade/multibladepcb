#include <Wire.h>
#include <Adafruit_MS8607.h>
#include <ThingSpeak.h>
#include <SPI.h>
#include <Ethernet.h>
#include <LiquidCrystal_I2C.h>

// Initialize the LCD with the I2C address (0x3F) and dimensions (16 columns, 2 rows)
LiquidCrystal_I2C lcd(0x27, 16, 2);
#define DEBUG 1

byte mac[] = { 0xD4, 0x28, 0xB2, 0xFF, 0xA0, 0xA7 };   // Replace with your Ethernet shield MAC address
EthernetClient client;

Adafruit_MS8607 sensorA; // Sensor object for the left sensor
Adafruit_MS8607 sensorB; // Sensor object for the right sensor

int pinA = 8; // Transistor control pin for left sensor
int pinB = 9; // Transistor control pin for right sensor
bool selectSensor = true;

// ThingSpeak Channel details
unsigned long channelID = 2221640;         // Replace with your ThingSpeak channel ID
const char* writeAPIKey = "BY1ECRI6WNPOLVBV";   // Replace with your ThingSpeak write API key
const int updateThingSpeakInterval = 60 * 1000;   

long lastConnectionTime=0;
bool lastConnected=false;
int failedCounter=0;


void setup()
{
  Serial.begin(115200);
  Serial.println("========================MS8607 Test=====================");
  Serial.println("========================Arduino Nano Every==============");
  Serial.println("MS8607 Sensor test");

  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);

  digitalWrite(pinA, LOW); // Turn off left sensor initially
  digitalWrite(pinB, HIGH); // Turn off right sensor initially

  delay(250);

  if (!sensorA.begin() || !sensorB.begin()) {
    Serial.println("Sensor(s) not found :(");
    while (1);
  }
  Serial.println("Sensors Found");

  lcd.init();
  lcd.backlight();

  startEthernet();
 
}

void startEthernet()
{
  
  client.stop();
  Serial.println("Connecting Arduino to network...");
  Serial.println();  
  delay(1000);
  
  // Connect to network amd obtain an IP address using DHCP
  if (Ethernet.begin(mac) == 0)
  {
    Serial.println("DHCP Failed, reset Arduino to try again");
    Serial.println();
  }
  else
  {
    Serial.println("Arduino connected to network using DHCP");
    Serial.println();
  }
  
  delay(1000);
}
void updateThingSpeak(String tsData) {
  if (client.connect("api.thingspeak.com", 80)) {
    client.print("POST /update HTTP/1.1\r\n");
    client.print("Host: api.thingspeak.com\r\n");
    client.print("Connection: close\r\n");
    client.print("X-THINGSPEAKAPIKEY: BY1ECRI6WNPOLVBV\r\n"); // Replace with your ThingSpeak API key
    client.print("Content-Type: application/x-www-form-urlencoded\r\n");
    client.print("Content-Length: ");
    client.print(tsData.length());
    client.print("\r\n\r\n");
    client.print(tsData);

    lastConnectionTime=millis();
    
    if(client.connected()){
      Serial.println("Connecting to ThingSpeak...");
      Serial.println();
      failedCounter=0;
    }else {
      failedCounter++;
      Serial.println("Connection to ThingSpeak failed("+ String(failedCounter, DEC)+")");
      Serial.println();
      lastConnectionTime=millis();
    }
  }

}


void loop()
{
  while (client.available())
  {
    char c = client.read();
    Serial.print(c);
  }
  
//DISCONNECT FROM THING SPEAK
    if(!client.connected()&& lastConnected)
    {
      Serial.println("..disconnected");
      Serial.println();
      client.stop();
    }   

  if (selectSensor)
  {
    digitalWrite(pinA, LOW); // Turn on left sensor
    digitalWrite(pinB, HIGH); // Turn off right sensor
    sensors_event_t temp, pressure, humidity;
    sensorA.getEvent(&pressure, &temp, &humidity);
    // Print sensor readings to the Serial monitor
    Serial.print("Sensor 1 - Temperature: "); Serial.print(temp.temperature); Serial.println(" degrees C");
    Serial.print("Sensor 1 - Pressure: "); Serial.print(pressure.pressure); Serial.println(" hPa");
    Serial.print("Sensor 1 - Humidity: "); Serial.print(humidity.relative_humidity); Serial.println(" %rH");
    Serial.println("");

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("T1: ");
    lcd.print(temp.temperature,1); // Temperature value for sensor 1
    lcd.setCursor(0, 1);
    lcd.print("P1: ");
    lcd.print(pressure.pressure,1); // Pressure value for sensor 1
    lcd.setCursor(8, 0);
    lcd.print("H1: ");
    lcd.print(humidity.relative_humidity,1); // Humidity value for sensor 1

 if ((!DEBUG && !client.connected() && (millis() - lastConnectionTime > updateThingSpeakInterval)) || 
        (DEBUG && !client.connected() && (millis() - lastConnectionTime > 16*1000 )))
  {
    updateThingSpeak("&field1="+String(temp.temperature, DEC)+"&field3="+String(pressure.pressure, DEC)+"&field5="+String(humidity.relative_humidity, DEC));
  }
    selectSensor = false;
  }
  else
  {
    digitalWrite(pinA, HIGH); // Turn off left sensor
    digitalWrite(pinB, LOW); // Turn on right sensor
    
    sensors_event_t temp2, pressure2, humidity2;
    sensorB.getEvent(&pressure2, &temp2, &humidity2);

    // Print sensor readings to the Serial monitor
    Serial.print("Sensor 2 - Temperature: "); Serial.print(temp2.temperature); Serial.println(" degrees C");
    Serial.print("Sensor 2 - Pressure: "); Serial.print(pressure2.pressure); Serial.println(" hPa");
    Serial.print("Sensor 2 - Humidity: "); Serial.print(humidity2.relative_humidity); Serial.println(" %rH");
    Serial.println("");

    lcd.setCursor(0,0);
    lcd.print("T2: ");
    lcd.print(temp2.temperature,1); // Temperature value for sensor 1
    lcd.setCursor(0, 1);
    lcd.print("P2: ");
    lcd.print(pressure2.pressure,1); // Pressure value for sensor 1
    lcd.setCursor(8, 0);
    lcd.print("H2: ");
    lcd.print(humidity2.relative_humidity,1); // Humidity value for sensor 1

    if ((!DEBUG && !client.connected() && (millis() - lastConnectionTime > updateThingSpeakInterval)) || 
        (DEBUG && !client.connected() && (millis() - lastConnectionTime > 16*1000 )))
  {
    updateThingSpeak("&field2="+String(temp2.temperature, DEC)+"&field4="+String(pressure2.pressure, DEC)+"&field6="+String(humidity2.relative_humidity, DEC));
  }
    selectSensor = true;
 
  }
  delay(2000);
//CHECK IF ARDUINO NEEDS TO BE RESTARTED

     if(failedCounter>3){
       startEthernet();
       lastConnected=client.connected();
     }

  // Write data to ThingSpeak
  
}
